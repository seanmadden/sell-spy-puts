using System;
using System.Linq;
using QuantConnect.Data;
using QuantConnect.Securities.Option;

namespace QuantConnect.Algorithm.CSharp
{
    public class Optionschaindebug : QCAlgorithm
    {
        private DateTime _liquidationDate;
        public override void Initialize()
        {
            SetStartDate(2021, 01, 06);
            SetEndDate(2021, 12, 31);
            SetCash(100000);

            SetWarmUp(TimeSpan.FromDays(2));

            var spy = AddOption("SPY");
            spy.SetFilter(TimeSpan.FromDays(20), TimeSpan.FromDays(61));
            
            // Needed to calculate options Greeks
            // These are calculated by the engine, apparently:
            // https://www.quantconnect.com/forum/discussion/3039/value-of-greeks-delta-is-always-0/p1
            spy.PriceModel = OptionPriceModels.CrankNicolsonFD();
        }

        /// OnData event is the primary entry point for your algorithm. Each new data point will be pumped in here.
        /// Slice object keyed by symbol containing the stock data
        public override void OnData(Slice data)
        {
            if (IsWarmingUp) return;

            if (Portfolio.Invested)
            {
                // Liquidate the puts at 21 DTE no matter what
                if (Time > _liquidationDate)
                {
                    Log($"{Time} Liquidating!");
                    Liquidate();
                    _liquidationDate = DateTime.MaxValue;
                }

                return;
            }

            foreach (var chain in data.OptionChains)
            {
                var contracts = chain.Value.Contracts
                    .Where(x => x.Key.ID.OptionRight == OptionRight.Put &&
                                    Math.Abs(x.Value.Greeks.Delta) <= 0.16m &&
                                    x.Value.Expiry.AddDays(-21) > Time
                    )
                    .OrderBy(x => x.Value.Greeks.Delta)
                    .ToList();

                if (contracts.Count == 0)
                {
                    return;
                }

                var sixteenDeltaContract = contracts.First();

                if(!Portfolio.Invested) {
                    Log($"{Time} selling a put");
                    Sell(sixteenDeltaContract.Key, 100);
                    _liquidationDate = sixteenDeltaContract.Value.Expiry.AddDays(-21);
                }
            }
        }
    }
}